import { ADD_MESSAGE } from "../Action/Types";

const intialState=[]

export function AddMsg(state=intialState,action) {
 switch(action.type){
    case ADD_MESSAGE:
        return state.concat([
            {
         message: action.message,
        }])
      
    default:
        return state  
    }

}