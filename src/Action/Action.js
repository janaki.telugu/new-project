import { ADD_MESSAGE, REC_MESSAGE,USER_MESSAGE } from "./Types";

var messageAdd=0
var messageRec=0

export const addMessage = (message) => ({
    type: ADD_MESSAGE,
    id: messageAdd++,
    message,
    
  })
  

  export const receivedMessage=(message)=>({
    type: REC_MESSAGE,
    id:messageRec++,
    message,
   
  })

  export const userMessage=(user)=>({
    type: USER_MESSAGE,
    user,
   
  })