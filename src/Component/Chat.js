import { Button, Grid, TextField, Typography } from '@mui/material'
import axios from 'axios'
import React,{useState} from 'react'
import {useDispatch,useSelector} from 'react-redux'
import { addMessage,receivedMessage ,userMessage} from '../Action/Action'

function Chat() {

const dispatch =useDispatch()
const[message,setMessage]=useState("")

const[user,setUser]=useState("person1")

const handleUser=(e)=>{
    setUser(e.target.value)
}
// console.log(user)

const handleMessage=(e)=>{
    setMessage(e.target.value)
dispatch(userMessage(user))

}


const data2=useSelector(state=>state.Reducer)
const data1=useSelector(state=>state.AddMsg)
const data3=useSelector(state=>state.User)

console.log(data3)



// console.log(data1)

const submitMessage=(e)=>{

    
    // e.preventDefault()


const newMsg={message}
dispatch(addMessage(message))
axios.post("https://fipolo-webhook-test.herokuapp.com/send",newMsg)
.then((res)=>dispatch(receivedMessage(res)))

}


return (
    <Grid container>
     <Grid sx={{height:600,width:200,border:2,backgroundColor:"lightskyblue"}} item lg={3} md={3} xs={12} sm={12}>
     <Typography sx={{marginTop:3,color:"black",fontWeight:900}}variant='h4'>UserList</Typography>
     <Button sx={{height:60,width:200,border:2,marginTop:10}} variant="contained" color="success" value="person1" onClick={handleUser}>Person1</Button>
     <Button sx={{height:60,width:200,border:2,marginTop:5}} variant="contained" color='primary' value="person2" onClick={handleUser}>Person2</Button>
     <Button sx={{height:60,width:200,border:2,marginTop:5}} variant="contained" color="success" value="person3" onClick={handleUser}>Person3</Button>
     <Button sx={{height:60,width:200,border:2,marginTop:5}} variant="contained" color='primary' value="person4" onClick={handleUser}>Person4</Button>
    </Grid>
    
     <Grid  sx={{height:600,width:200,border:2}} item lg={9} md={9} xs={12} sm={12}>
     <Grid  sx={{height:40,width:"100%",border:2,display:"flex",justifyContent:"space-around",backgroundColor:"peachpuff"}} item lg={12} md={12} xs={12} sm={12}>
     <Typography variant='h5'sx={{color:"red",fontWeight:800,fontStyle:"italic"}}>Chat box</Typography>
     <Typography variant='h5' sx={{color:"blue",fontWeight:800}}><i class="fa-regular fa-user"></i>{user}</Typography>
     <Typography variant='h5'><i class="fa-solid fa-phone"></i></Typography>
     <Typography variant='h5'><i class="fa-solid fa-video"></i></Typography>
     <Typography variant='h5'><i class="fa-solid fa-gear"></i></Typography>
     

      
      </Grid>
      <Grid  sx={{height:500,width:"100%",border:2}} item lg={12} md={12} xs={12} sm={12}>
        {data1.map((val)=><h2 style={{color:"blue",textAlign:"left"}}>{val.message}</h2>)}
       {data2.map((value)=> <h2 style={{color:"black",textAlign:"right"}}>{value.message.data.reply}</h2>)}
      </Grid>
      <Grid  sx={{height:58,width:"100%",border:2,display:"flex",justifyContent:"space-between",backgroundColor:"peachpuff"}} item lg={12} md={12} xs={12} sm={12}>
     <TextField sx={{heigth:40,width:400,color:"blue"}} placeholder='Message Me' name="message" onChange={handleMessage}/>
     <Button variant="contained"color="secondary"><i class="fa-regular fa-photo-film"></i></Button>

     <Button variant="contained" color='primary' onClick={submitMessage}><i class="fa-solid fa-arrow-right"></i></Button>
     </Grid>
     </Grid>

    </Grid>
  )
}

export default Chat